from django.conf.urls import patterns, include, url
from django.contrib import admin
from polls import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^polls/', include('polls.urls', namespace="polls")),
                       url(r'^mysiteadmin/', include(admin.site.urls)),
                       )
