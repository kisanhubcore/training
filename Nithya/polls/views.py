from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from models import Question

def index(request):
    template_name = 'polls/index.html'
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, template_name, context)

def detail(request, question_id):
    template_name = 'polls/detail.html'
    question = get_object_or_404(Question, pk=question_id)
    return render(request, template_name, {'question': question})

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)