from django.db import models

class Individual(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

class MyPerson(Individual):
    class Meta:
        proxy = True

    def do_something(self):
        pass

class Chapter(models.Model):
    chapter_id = models.AutoField(primary_key=True)
    headline = models.CharField(max_length=50)
    body = models.TextField()

class Book(models.Model):
    book_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50)

class BookReview(Book, Chapter):
    pass